<?php require_once(ROOT_PATH . "/views/header.php"); ?>
    <main role="main">

        <section class="jumbotron text-center">
            <div class="container">
                <h1>Test Shop</h1>
                <p>
                    Регистрация
                </p>
                <p>
                    <a href="/index.php" class="btn btn-secondary my-2">Go to Products</a>
                </p>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">
                <form class="formsignin" action="formChangePassword.php" method="post" enctype="multipart/form-data">
                    <p class="h5 mb-3 font-weight-normal">Заполните поля для регистрации</p>
                    <?php if (!empty($error)): ?>
                    <p style="color: crimson">
                        <?php echo $error;
                        endif; ?>
                    </p>
                    <input class="form-control" type="text" name="login" placeholder="Логин">
                    <input class="form-control" type="password" name="old_pass" placeholder="Старый пароль">
                    <input class="form-control" type="password" name="new_pass" placeholder="Новый пароль">
                    <input class="form-control" type="password" name="new_pass_conf"
                           placeholder="Повторите пароль"><br/>
                    <p>
                        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Сменить пароль
                        </button>
                    </p>
                    <p>
                        <a href="/formLogin.php">Войти!</a>
                    </p>

                </form>
            </div>
        </div>

    </main>
<?php require_once(ROOT_PATH . "/views/footer.php"); ?>