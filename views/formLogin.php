<?php require_once(ROOT_PATH . "/views/header.php"); ?>
    <main role="main">

        <section class="jumbotron text-center">
            <div class="container">
                <h1>Test Shop</h1>
                <p>
                    Войдите, чтобы добавить покупки в корзину
                </p>
                <p>
                    <a href="/index.php" class="btn btn-secondary my-2">Go to Products</a>
                </p>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">
                <form class="formsignin" action="formLogin.php" method="post" enctype="multipart/form-data">
                    <p class="h3 mb-3 font-weight-normal">Выполните вход</p>
                    <?php if (!empty($error)): ?>
                    <p style="color: crimson">
                        <?php echo $error;
                        endif; ?>
                    </p>
                    <input class="form-control" type="text" name="login" placeholder="Логин">
                    <input class="form-control" type="password" name="password" placeholder="Пароль"><br/>
                    <div>
                        <label>
                            <input class="checkbox mb-3" type="checkbox" name="remember" placeholder="Пароль">Запомнить
                            меня
                        </label>
                    </div>
                    <p>
                        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Вход</button>
                    </p>
                    <p>
                        <a href="/formRegistration.php">Зарегистрироваться!</a>
                    </p>
                    <p>
                        <a href="/formChangePassword.php">Сменить пароль</a>
                    </p>


                </form>
            </div>
        </div>

    </main>
<?php require_once(ROOT_PATH . "/views/footer.php"); ?>