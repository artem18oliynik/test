<?php

    require_once dirname(__FILE__) . "/config.php";
    require_once dirname(__FILE__) . "/functions.php";

    //$products = json_decode(file_get_contents(ROOT_PATH . "/products.json"), true);
    //    if (!empty($_GET['category']) && in_array($_GET['category'], getCategoryId($pdo)) == false ){ // если введенная категория в урле не соответствует значениям в бд
    //        header("Location: /index.php");
    //        exit;
    //    }

    if (!empty($_POST['category'])) {
        $products = [];
        foreach ($_POST['category'] as $k => $category) {
            $result = getProductsByCategory($pdo, $category);
            $products = array_merge($products, $result);
        }
    } else {
        $products = getAllProducts($pdo);
    }
    $categories = getAllCategories($pdo);
    if (!empty($_POST['products'])) {
        foreach ($_POST['products'] as $product) {
            $_SESSION['products'][] = ["id" => $product, "quantity" => 1];
        }
        header("Location: /cart.php");
        exit;
    }

    require_once dirname(__FILE__) . "/testPagin.php"; // пагинация

    require_once dirname(__FILE__) . "/views/products.php";

?>
