<?php

    function arrayUsers($fileName) // принимает имя файла и возвращает массив с пользователями
    {
        if (!file_exists($fileName)) {
            return [];
        }
        $json = file_get_contents($fileName);
        return json_decode($json, true);
    }

    function dataUser($login, $pass, $arr) // принимает логин, пароль и массив пользователей и возвращает данные пользователя с указанным логином и паролем
    {
        if (empty($login) || empty($pass) || empty($arr)){
            return false;
        }
        foreach ($arr as $user) {
            if ($user["login"] == $login && $user["password"] == $pass) {
                return $user;
            }
        }
        return "Пользователь не найден";
    }

    $file = dirname(__FILE__) . "/dataUsers.json";
    $arrayUser = arrayUsers($file); // выполнение первой функции

    $login = "login3";
    $password = "qwerty";
    print_r(dataUser($login, $password, $arrayUser)); // выполнение второй функции

?>