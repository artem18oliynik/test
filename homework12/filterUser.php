<?php
    require_once dirname(__FILE__) . "/config.php";

    require ROOT_PATH . "/dataUserArray.php";

    $arrFilterUsers = []; // выбираем пользователей у которых пароль меньше 8 символов
    foreach ($arrDataUsers as $user){
        if (strlen(trim($user["password"])) < 8){
            $arrFilterUsers[] = $user;
        }
    }

?>