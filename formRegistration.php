<?php
    require_once dirname(__FILE__). "/config.php";
    require_once dirname(__FILE__). "/functions.php";

    if (!empty($_SESSION['userData']))  {
        header("Location: /index.php");
        exit;
    }

    if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['login']) && !empty($_POST['password'])) {
        $response = registrationRequest($_POST['name'], $_POST['email'], $_POST['login'], $_POST['password']);
        if(!empty($response) && $response['success'] == 1){
            $_SESSION['userData'] = $response['data'];
            header("Location: /index.php");
        } else{
            $error = $response['error'];
        }

    }
    require_once dirname(__FILE__) . "/views/formRegistration.php";
?>
