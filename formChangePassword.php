<?php

    require_once dirname(__FILE__) . "/config.php";
    require_once dirname(__FILE__) . "/functions.php";

    if (!empty($_POST['login']) && !empty($_POST['old_pass']) && !empty($_POST['new_pass']) && !empty($_POST['new_pass_conf'])) {
        $response = changePassRequest($_POST['login'], $_POST['old_pass'], $_POST['new_pass'], $_POST['new_pass_conf']);
        if (!empty($response) && $response['success'] == 1) {
            $_SESSION = $response['data'];
            if (!empty($_POST['remember'])) {
                SetCookie("name", $response['data']['name'], time() + 3600 * 24);
            }
            header("Location: /main.php");
        } else {
            $error = $response['error'];
        }
    }
    require_once dirname(__FILE__) . "/views/formChangePassword.php";
?>
