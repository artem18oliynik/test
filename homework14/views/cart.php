<?php
    require_once(dirname(__FILE__, "2") . "/config.php");
    require_once(ROOT_PATH . "/views/header.php");

    $products = json_decode(file_get_contents(ROOT_PATH . "/products.json"), true);
    //var_dump($products); die();



    if (!empty($_POST["order"]) && !empty($_SESSION['products'])){ // Delete with Cart
        //var_dump($_SESSION['products']); die();
        foreach ($_SESSION['products'] as $key => $product){
            if (in_array($product, $_POST["order"])){
                unset($_SESSION['products'][$key]);
            }
        }unset($product);
    }
    $cartProducts = []; // Товары в корзине
    foreach ($products as &$value){
        if (in_array($value["id"], $_SESSION['products'])){
            $cartProducts[] = $value;
        }
    }unset($value);

    //file_put_contents(ROOT_PATH . "/products.json", json_encode($products));




?>

    <main role="main">

        <section class="jumbotron text-center">
            <div class="container">
                <h1>Test Shop</h1>
                <p class="lead text-muted">Something short and leading about the collection below—its contents, the
                    creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it
                    entirely.</p>
                <p>
                    <a href="/homework14/index.php" class="btn btn-secondary my-2">Go to Products</a>
                </p>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">
                <form method="POST" action="">
                    <?php if (empty($_SESSION['products'])): ?>
                        <div>
                            <p>Your cart is empty!</p>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <?php foreach ($cartProducts as $cartProduct): ?>
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                                         xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                                         focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>
                                            Placeholder</title>
                                        <rect width="100%" height="100%" fill="#55595c"/>
                                        <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                                    </svg>
                                    <div class="card-body">
                                        <p class="card-text"> <?php echo $cartProduct["name"] ?></p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group chechbox-block">
                                                <label class="form-check-label" for="exampleCheck1">Check me</label>
                                                <input class="form-check-input" type="checkbox" name="order[]" value="<?php echo $cartProduct['id']; ?>">

                                            </div>
                                            <small class="text-muted">UAH <?php echo money_format('%i', $cartProduct['price']);?> </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="row">
                        <div class="col-md-4 center-block">
                        </div>
                        <div class="col-md-4 center-block">
                            <input type="submit" class="btn btn-primary order-button" value="Delete"/>
                        </div>
                        <div class="col-md-4 center-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </main>
<?php require_once(ROOT_PATH . "/views/footer.php"); ?>