<?php
    require_once dirname(__FILE__). "/config.php";
    require_once dirname(__FILE__). "/functions.php";

    $products = json_decode(file_get_contents(ROOT_PATH . "/products.json"), true);
    if (!empty($_POST['products'])) {
        $_SESSION['products'] = $_POST['products'];
        foreach ($products as &$value){
            if (in_array($value["id"], $_SESSION['products'])){
                $value['quantity'] --;
            }
        }unset($value);
        file_put_contents(ROOT_PATH . "/products.json", json_encode($products));
        header("location: /homework14/views/cart.php");
        exit;
    }
    require_once dirname(__FILE__) . "/views/products.php";
?>
