<?php

    function getAllProducts($db)
    {
        $stmt = $db->prepare("SELECT * FROM `products`");
        $stmt->execute();
        return $stmt->fetchAll();
    }
//
    function getProductsByCategory($db, $categories)
    {
        $stmt = $db->prepare("SELECT * FROM `products` WHERE category_id = :category_id");
        $stmt->execute(["category_id" => $categories]);
        return $stmt->fetchAll();
    }

    function getAllCategories($db)
    {
        $stmt = $db->prepare("SELECT * FROM `category`");
        $stmt->execute();
        return $stmt->fetchAll();
    }
    function getCategoryId($db)
    {
        $stmt = $db->prepare("SELECT id FROM `category`");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_COLUMN);
    }

    function generateProducts()
    {
        $products[] = ["id" => 1, "name" => "SAMSUNG TV", "price" => 15000, "quantity" => 5, "img" => "product1.jpg"];
        $products[] = ["id" => 2, "name" => "LG TV", "price" => 13000, "quantity" => 8, "img" => "product2.jpg"];
        $products[] = ["id" => 3, "name" => "Iphone XS", "price" => 13000, "quantity" => 8];
        $products[] = ["id" => 4, "name" => "Xbox", "price" => 10000, "quantity" => 10,  "img" => "product1.jpg"];
        $products[] = ["id" => 5, "name" => "PHP Programming", "price" => 1000, "quantity" => 2];
        file_put_contents("products.json", json_encode($products));
    }

    function sendLoginRequest($login, $password) // возвращает данные о пользователе при входе
    {
        $data = array("login" => $login, "password" => $password);
        $dataString = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_URL."login");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//для возврата результата в виде строки, вместо прямого вывода в браузер
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
        curl_close ($ch);
        $response = json_decode($response, true);
        return $response;
    }
    function registrationRequest($name, $email, $login, $password) // возвращает данные о пользователе при входе
    {
        $data = array("name" => $name, "email" => $email, "login" => $login, "password" => $password);
        $dataString = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_URL."registration");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//для возврата результата в виде строки, вместо прямого вывода в браузер
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
        curl_close ($ch);
        $response = json_decode($response, true);
        return $response;
    }

    function changePassRequest($login, $oldPass, $newPass, $newPassConf)
    {
        $data = array("login" => $login, "old_pass" => $oldPass, "new_pass" => $newPass, "new_pass_conf" => $newPassConf);
        $dataString = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_URL."change_pass");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//для возврата результата в виде строки, вместо прямого вывода в браузер
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
        curl_close ($ch);
        $response = json_decode($response, true);
        return $response;
    }

    function getCartProducts($db, $cartProducts)
    {
        if (empty($db) || empty($cartProducts)) {
            return [];
        }
        $products = getAllProducts($db);
        $productsCart = [];
        $ids = array_column($cartProducts, 'id');
        foreach ($products as $product) {
            if (in_array($product['id'], $ids)) {
                $product['selected_quantity'] = $cartProducts[array_search( // записываем в 'selected_quantity' значение 'quantity' из сессии
                    $product['id'],
                    $ids
                )]['quantity'];
                $productsCart[] = $product;
            }
        }
        return $productsCart;
    }

    function getProductQuantity($products, $id)
    {
        foreach($products as $product){
            if($product['id'] == $id){
                return $product['quantity'];
            }
        }
    }

    function getTotalPrice($products)
    {
        $sum = 0;
        foreach($products as $product){
            $sum += $product['price'] * $product['selected_quantity'];
        }
        return $sum;
    }

