<?php
    require_once dirname(__FILE__). "/config.php";
    require_once dirname(__FILE__). "/functions.php";
//    if (empty($_SESSION['products'])) {
//        header("Location: index.php");
//    }
    if (empty($_SESSION['userData'])) {
        header("location: formLogin.php");
    }

    if (!empty($_POST)) { // Обновляем значение 'quantity' в сессии
        foreach ($_POST as $k => $val){
            $id = explode("_", $k)[1];
            foreach($_SESSION['products'] as $key => &$product){
                if(!empty($product) && $product['id'] == $id){
                    if($val == 0){
                        array_splice($_SESSION['products'], $key, 1);
                    } else {
                        $product['quantity'] = $val;
                    }
                }
            } unset($product);
        }
    }

    if (!empty($_SESSION['products'])) {
        $products = getCartProducts($pdo, $_SESSION['products']);
        $totalPrice = getTotalPrice($products);
    }

    if (!empty($_GET['action']) && $_GET['action'] == 'buy') { // При покупке отнимать количество товаров в файле и очищать корзину
        $productsFile = json_decode(file_get_contents(ROOT_PATH . "/products.json"), true);
        $ids = array_column($products, 'id');
        foreach ($productsFile as &$product){
            if (in_array($product['id'], $ids)){
                $product['quantity'] = (int)$product['quantity'] - (int)$products[array_search($product['id'], $ids)]['selected_quantity'];
            }
        }unset($product);
        $productsFile = json_encode($productsFile);
        file_put_contents(ROOT_PATH . "/products.json", $productsFile);
        unset($_SESSION['products']);
        header("location: cart.php");
        exit;

    }
    require_once dirname(__FILE__) . "/views/cart.php";
?>
