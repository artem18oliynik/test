<?php
define("ROOT_PATH", dirname(__FILE__));

if(!empty($_POST['login']) && !empty($_POST['password'])) {
    $login = $_POST['login'];
    $password = $_POST['password'];

    $file = fopen(ROOT_PATH . "/logpass.txt", "r");
    if (!$file) {
        die("Error");
    }
    while (!feof($file)) {
        $arr = explode(" ", fgets($file));
        $count = 0; // счетчик, если будет равен 0, то введенный логин и пароль не совпадает с теми которые в файле
        if ($arr[0] == $login && trim($arr[1]) == $password) { // trim потому что появляются пробелы в конце пароля (не знаю че так..)
            if (file_exists(ROOT_PATH . "/$arr[0].txt")) { // если файл с таким названием существует
                $k = (int)file_get_contents(ROOT_PATH . "/$arr[0].txt"); // значение которое в файле перевожу в int и добавляю 1
                $k++;
                file_put_contents(ROOT_PATH . "/$arr[0].txt", "$k"); // записываю в файл новое значение
            } else {
                file_put_contents(ROOT_PATH . "/$arr[0].txt", "1"); // иначе создаю файл и записываю в него 1
            }
            echo "Логин и пароль верны";
            $count++;
            break;
        }
    }
    if ($count == 0){
        echo "Неверный логин или пароль";
    }
    fclose($file);
//    header("Location: index.php");
//    exit();
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form</title>
</head>
<body>
<form method="post">
    <p>Логин</p>
    <input type="text" name="login" placeholder="Логин или e-mail">
    <p>Пароль</p>
    <input type="password" name="password" placeholder="Пароль">
    <p>
        <button type="submit" name="submit">Вход</button>
    </p>
</form>
</body>
</html>
