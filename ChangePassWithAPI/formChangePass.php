<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>FormChangePass</title>
</head>
<body>
<form class="form-signin" method="post" enctype="multipart/form-data">
    <h1 class="h3 mb-3 font-weight-normal">Смена пароля</h1>
    <input class="form-control" type="text" name="login" placeholder="Логин">

    <input class="form-control" type="password" name="old_pass" placeholder="Старый пароль">

    <input class="form-control" type="password" name="new_pass" placeholder="Новый пароль">

    <input class="form-control" type="password" name="new_pass_conf" placeholder="Повторите пароль"><br>

    <p>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Сменить пароль</button>
    </p>
    <p>
        <a href="index.php">Вход</a>
    </p>
    <?php if(!empty($error)): ?>
        <p>
            <?php echo $error; ?>
        </p>
    <?php endif; ?>
</form>
</body>
</html>
