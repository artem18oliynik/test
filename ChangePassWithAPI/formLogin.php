<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Form</title>
</head>
<body>
<form class="form-signin" action="index.php" method="post" enctype="multipart/form-data">
    <h1 class="h3 mb-3 font-weight-normal">Выполните вход</h1>
    <input class="form-control" type="text" name="login" placeholder="Логин">
    <input class="form-control" type="password" name="password" placeholder="Пароль"><br/>
    <div>
        <label>
            <input class="checkbox mb-3" type="checkbox" name="remember" placeholder="Пароль">Запомнить меня
        </label>
    </div>
    <p>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Вход</button>
    </p>
    <p>
        <a href="changePassPHP.php">Сменить пароль</a>
    </p>
    <?php if(!empty($error)): ?>
    <p>
        <?php echo $error; endif;?>
    </p>

</form>
</body>
</html>