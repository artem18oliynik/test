<?php
    require_once dirname(__FILE__). "/config.php";
    if(!empty($_GET['action']) && $_GET['action'] == "logout"){
        session_destroy();
        header("Location: /index.php");
        exit;
    }
    if(empty($_SESSION)){
        if(!empty($_COOKIE['name'])){
            $_SESSION['name'] = $_COOKIE['name'];
            var_dump($_SESSION);
        } else {
            header("Location: /index.php");
            exit;
        }
    }
?>
<!DOCTYPE html>
<html>
<body>
<h1><?php echo $_SESSION['name']; ?></h1>
<a href="/main.php?action=logout">Logout</a>
</body>
</html>