<?php
    require_once dirname(__FILE__). "/config.php";
    require_once dirname(__FILE__). "/functions.php";

    if (!empty($_POST['login']) && !empty($_POST['password'])) {
        $response = sendLoginRequest($_POST['login'], $_POST['password']);
        if(!empty($response) && $response['success'] == 1){
            $_SESSION = $response['data'];
            if(!empty($_POST['remember'])){
                SetCookie("name", $response['data']['name'], time()+3600*24);
            }
            header("Location: /main.php");
        } else{
            $error = $response['error'];
        }
    }

    require_once ROOT_PATH."/formLogin.php";
?>

