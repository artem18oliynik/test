<?php

    function sendLoginRequest($login, $password)
    {
        $data = array("login" => $login, "password" => $password);
        $dataString = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_URL."login");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//для возврата результата в виде строки, вместо прямого вывода в браузер
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
        curl_close ($ch);
        $response = json_decode($response, true);
        return $response;
    }

    function changePassRequest($login, $oldPass, $newPass, $newPassConf)
    {
        $data = array("login" => $login, "old_pass" => $oldPass, "new_pass" => $newPass, "new_pass_conf" => $newPassConf);
        $dataString = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_URL."change_pass");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//для возврата результата в виде строки, вместо прямого вывода в браузер
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
        curl_close ($ch);
        $response = json_decode($response, true);
        return $response;
    }
